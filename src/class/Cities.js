import React, { Component } from 'react';

class Cities extends Component {
  constructor(props) {
    super(props);
    this.state = {
      city : this.props.value,
    };
    this.handleClick = this.handleClick.bind(this);
  }
  render() {
    var ciudad = [];
    var aStyle = {
      cursor: 'pointer'
    }
    let city = [];
    if(this.props.value.length!==0 && this.props.value[0] !==""){
      this.props.value.map(function(object,i){
        //console.log(object);
        ciudad.push(
          <div className="card" key={i}>
            <div className="card-block text-center">
              <a style={aStyle} onClick={() => document.getElementById('city').value=object}><h3 >{object}</h3></a>
            </div>
          </div>
        );
      });

      return (
        <div className="col-xs-12">
          {ciudad}
        </div>
      );

    }else{
      return (
        <h1>No Tengo ciudades</h1>
      );
    }

  }
  // handleClick(i){
  //   console.log(i);
  // }
  handleClick = props=>{
    var input = document.getElementById('city');
    console.log(props);
    input.value=props;
  }
}



export default Cities;
