import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link, IndexRoute } from "react-router-dom";
import './index.css';
import App from './App';
import Search from './class/Search';
import About from './class/About';
import registerServiceWorker from './registerServiceWorker';
// const Routes = () =>(
//   <Router>
//     <div>

//
//       <hr />
//
//       <Route path="/about" component={About} />
//     </div>
//   </Router>
// );
// const About = () => (
//   <div>
//     <h2>About</h2>
//   </div>
// );

ReactDOM.render(
  <Router>
    <div>
      <ul>
        <li>
          <Link to="/search">Buscador</Link>
        </li>
        <li>
          <Link to="/about">About</Link>
        </li>
      </ul>
      <Route path="/search" component={Search}>
      </Route>
      <Route path="/about" component={About}>
      </Route>
    </div>
  </Router>,
  document.getElementById('container')
);
//ReactDOM.render(<Routes />, document.getElementById('rutas'));
registerServiceWorker();
